# Wake park service

**Microservices**

1. Auth microservice
2. Bar microservice
3. Payment microservice
4. Equipment microservice
5. Training microservice
6. Blog microservice
7. Seo microservice

There is a microservice architecture.

**Advantages of microservices**

1. Isolation
> Each micro service is isolated and independent other parts of applications.

2. Scalability
> Easier for development teams to scale up or down following the requirements of a specific element

3. Productivity
> Small databases, much less requests to server and easy to expand an application, when compared to monolithic system

4. Smaller and faster deployments
> Smaller codebases and scope = quicker deployments

**Disadvantages of microservices**
1. Global testing is difficult
2. Communication between services is complex


## How system works

### Customer

Client ask `Auth microservice` for permissions or create new account. Then app allow/(don't allow) customer to get in a Wake park and mark client as `authorized` with role `Client`.

Marked client can ask for `Bar microservice`, `Training microservice`, `Blog microservice`, `Equipment microservice` or `Payment microservice`.

### Employee

Employee ask `Auth microservice` for permissions. Then app allow/(don't allow) employee to get in a Wake park and mark employee as `authorized` with role `Admin`, `Trainer` or `Barmen`.

`Barmen` can ask for `Bar microservice` or `Payment microservice` if he needs to pay for ordering on bar.
`Trainer` can ask for `Training microservice` for CRUD trainings
`Admin` can ask for all microservice and configure each of them, if needed.