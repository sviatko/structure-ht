## Equipment microservice

1. Catalog service
    - sale / rental boards
2. Reserve service
    - Equipment rental reservation
3. Trace service
    - CRUD traces
4. Beach management
    - massage / bar / snacks
    - handling sunbeds ( clean up, take in/out )
